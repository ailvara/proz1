<?php

namespace Sylwia\MoviesBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sylwia\MoviesBundle\Entity\Movie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class MovieController extends FOSRestController implements ClassResourceInterface
{
    /**
    * @Rest\View()
    * @ApiDoc(
    *  description="Lista wszystkich filmów"
    * )
    */
    public function cgetAction()
    {
        $movies = $this->getDoctrine()->getManager()->getRepository('SylwiaMoviesBundle:Movie')
            ->findAll();
        return $movies;
    }
    
    /**
    * @Rest\View()
    * @ApiDoc(
    *  description="Lista filmów z danego gatunku"
    * )
    */
    public function cgetGenreAction($genre)
    {
        $movies = $this->getDoctrine()->getManager()->getRepository('SylwiaMoviesBundle:Movie')
            ->findBy(['genre' => $genre]);
        return $movies;
    }
    
    /**
    * @Rest\View()
    * @ApiDoc(
    *  description="Lista gatunków"
    * )
    */
    public function cgetGenresAction(){
        return [
            'animowany', 'dramat', 'komedia', 'musical', 'SF'
        ];
    }
    
    /**
     * @Rest\View()
     * @ApiDoc(
     *  description="Pobierz szczegóły filmu"
     * )
     */
    public function getAction($id)
    {
        $movie = $this->getDoctrine()->getManager()->getRepository('SylwiaMoviesBundle:Movie')
            ->findOneById($id);
        if($movie == null){
            throw new HttpException(404, "Nie ma takiego filmu.");
        }
        return $movie;
    }
    
    /**
     * @Rest\View()
     * 
     * @ApiDoc(
     *  description="Dodaj nowy film",
     *  parameters={
     *      {"name"="title", "dataType"="String", "required"=true},
     *      {"name"="year", "dataType"="integer", "required"=true},
     *      {"name"="genre", "dataType"="String", "required"=false},
     *      {"name"="country", "dataType"="String", "required"=false},
     *      {"name"="director", "dataType"="String", "required"=false}
     *  }
     * )
     */
    public function postAction(Request $request)
    {
        $movie = new Movie();
        $data = $request->request;
        $movie->setTitle($data->get('title'));
        $movie->setYear(intval($data->get('year')));
        $movie->setGenre($data->get('genre'));
        $movie->setCountry($data->get('country'));
        $movie->setDirector($data->get('director'));
        $this->getDoctrine()->getManager()->persist($movie);
        $this->getDoctrine()->getManager()->flush();
        return $movie;
    }
    
    /**
     * @Rest\View()
     * 
     * @ApiDoc(
     *  description="Zmień dane filmu",
     *  parameters={
     *      {"name"="title", "dataType"="String", "required"=false},
     *      {"name"="year", "dataType"="integer", "required"=false},
     *      {"name"="genre", "dataType"="String", "required"=false},
     *      {"name"="country", "dataType"="String", "required"=false},
     *      {"name"="director", "dataType"="String", "required"=false}
     *  }
     * )
     */
    public function putAction(Request $request, $id)
    {
        $movie = $this->getDoctrine()->getManager()->getRepository('SylwiaMoviesBundle:Movie')
            ->findOneById($id);
        if($movie == null){
            throw new HttpException(404, "Nie ma takiego filmu.");
        }
        $data = $request->request;
        
        if($data->get('title') != null) $movie->setTitle($data->get('title'));
        if($data->get('year') != null) $movie->setYear(intval($data->get('year')));
        if($data->get('genre') != null) $movie->setGenre($data->get('genre'));
        if($data->get('country') != null) $movie->setCountry($data->get('country'));
        if($data->get('director') != null) $movie->setDirector($data->get('director'));
        $this->getDoctrine()->getManager()->persist($movie);
        $this->getDoctrine()->getManager()->flush();
        return $movie;
    }
    
    /**
     * @Rest\View()
     * @ApiDoc(
     *  description="Usuń film"
     * )
     */
    public function deleteAction($id){
        $movie = $this->getDoctrine()->getManager()->getRepository('SylwiaMoviesBundle:Movie')
            ->findOneById($id);
        if($movie == null){
            throw new HttpException(404, "Nie ma takiego filmu.");
        }
        $this->getDoctrine()->getManager()->remove($movie);
        $this->getDoctrine()->getManager()->flush();
        return ["message" => "Usunięto"];
    }
}
