use Symfony\Component\Form\FormFactoryInterface;

class PageHandler {

    private $om;
    private $entityClass;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->formFactory = $formFactory;
    }
    
    public function all()
    {
        return $om->getRepository('SylwiaMoviesBundle:Movie')->findAll();
    }
}